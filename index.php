<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Google Doc Proxy</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0"/>
<meta name="description" content="It delivers your Google Doc as appropriate HTML tags to anywhere you want.">
<meta name="author" content="Hiro Nozu">
<link rel="shortcut icon" href="images/favicon.ico">
<!-- <script>document.cookie='resolution='+Math.max(screen.width,screen.height)+'; path=/';</script> -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- <link href="application.css" rel="stylesheet"> -->
<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-39856458-8', 'hironozu.com');
  ga('send', 'pageview');

</script>
</head>
<body>
<div class="container-fluid">

  <div id="content" class="row-fluid">

    <h1>Google Doc Proxy</h1>

    <p>Google Doc Proxy let you get your Google Doc with HTML tags to embed it to your website directly.</p>

    <p>Google Doc Proxy provides two ways of delivery type. Via JavaScript or PHP. It is good to choose the way with PHP because the content actually looks like a part of your website. However, it is easier to use with JavaScript a bit.</p>

    <p>Google Doc Proxy is powered by <a target="_blank" title="PHP class to export Google Doc" href="https://github.com/hironozu/google_doc_exporter_demo">Google Doc Exporter</a> - <a title="Export Google Doc" href="http://public.hironozu.com/google_doc_exporter_demo/">See the power of Google Doc Exporter</a></p>

  </div>

</div>
</body>
</html>
